package com.example.android.intentdemo;

import static com.example.android.intentdemo.AllKeys.KEY_ANAK;
import static com.example.android.intentdemo.AllKeys.KEY_NAMA;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class HomeActivity extends AppCompatActivity {
    final String TAG = "HomeActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Ini contoh menggunakan bundle
        Bundle b = getIntent().getExtras();
        String nama = b.getString(KEY_NAMA);
        int anak = b.getInt(KEY_ANAK, 0);

        //Ini contoh apabila Extra dibaca satu persatu
        //String nama = getIntent().getStringExtra(KEY_NAMA);
        //int anak = getIntent().getIntExtra(KEY_ANAK,0);

        Log.d(TAG,"Nama: "+nama+" Anak:"+anak);
    }

    public void selesaiActivity(View v) {
        finish();
    }
}