package com.example.android.intentdemo;

import static com.example.android.intentdemo.MainActivity.CHOOSE_FOOD_REQUEST;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ResultActivity extends AppCompatActivity {
    public static final String EXTRA_REPLY = "order_result";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
    }

    public void kembalikanResult(View v) {
        // Create an intent
        Intent replyIntent = new Intent();
        // Put the data to return into the extra
        replyIntent.putExtra(EXTRA_REPLY, "Bakpao");
        // Set the activity's result to RESULT_OK
        setResult(RESULT_OK, replyIntent);
        // Finish the current activity
        finish();
    }
}