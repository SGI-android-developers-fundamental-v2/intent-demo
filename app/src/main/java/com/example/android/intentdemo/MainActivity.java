package com.example.android.intentdemo;

import static com.example.android.intentdemo.AllKeys.KEY_ANAK;
import static com.example.android.intentdemo.AllKeys.KEY_NAMA;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    public static final int CHOOSE_FOOD_REQUEST = 1;
    private static final String TAG = "MainActivity";

    ActivityResultLauncher<Intent> someActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        String reply = data.getStringExtra(ResultActivity.EXTRA_REPLY);
                        Log.d(TAG,"Result dari ResultActivity adalah "+reply);
                    }
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void pindahActivity(View v){
        Intent i = new Intent(this, HomeActivity.class);
        i.putExtra(KEY_NAMA, "Dony");
        i.putExtra(KEY_ANAK, 3);
        startActivity(i);
    }

    public void pindahActivityDenganResult(View v){
        Intent intent = new Intent(this, ResultActivity.class);
        startActivityForResult(intent, CHOOSE_FOOD_REQUEST);
    }

    public void pindahActivityDenganResultNewWay(View v) {
        Intent intent = new Intent(this, ResultActivity.class);
        someActivityResultLauncher.launch(intent);
    }

    public void selesaiActivity(View v) {
        finish();
    }

    public void bukaBrowser(View v) {
        Uri uri = Uri.parse("http://www.google.com");
        Intent it = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(it);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHOOSE_FOOD_REQUEST) { // Identify activity
            if (resultCode == RESULT_OK) { // Activity succeeded
                String reply = data.getStringExtra(ResultActivity.EXTRA_REPLY);
                Log.d(TAG,"Result dari ResultActivity adalah "+reply);
            }
        }
    }
}